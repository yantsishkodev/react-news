import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPost } from "./../actions/index";
import { deletePost } from "./../actions/index";
import { Link } from 'react-router-dom';

class PostsShow extends Component {

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
  }

  onDeleteClick = () =>{
    const { id } = this.props.match.params;

    const { deletePost, history } = this.props;

    deletePost(id)
      .then(()=>{
        history.push('/');
    });
  };

  render() {

    const { post } = this.props;

    if (!post){
      return <div>Loading...</div>;
    }

    return (
      <div>
        <Link to="/">Back</Link>
        <button className="btn btn-danger pull-xs-right" onClick={this.onDeleteClick}>Delete Post</button>
        <h3>{post.title}</h3>
        <h6>{post.categories}</h6>
        <p>{post.content}</p>
      </div>
    );
  }
}

export default connect(({ posts }, ownProps)=>{
  return {
    post: posts[ownProps.match.params.id]
  }
}, {
  fetchPost,
  deletePost
})(PostsShow);