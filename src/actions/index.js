import axios from 'axios';

export const FETCH_POSTS = 'FETCH_POSTS';
export const CREATE_POST = 'CREATE_POST';
export const FETCH_POST = 'FETCH_POST';
export const DELETE_POST = 'DELETE_POST';

const ROOT_URL = 'http://reduxblog.herokuapp.com/api';
const API_KEY = '?key=12345aaaaaaaa';

export function fetchPosts () {
  const request = axios.get(`${ROOT_URL}/posts${API_KEY}`);

  return dispatch => {
    request.then(posts=>{
      dispatch({
        type: FETCH_POSTS,
        payload: posts
      })
    })
  };
}

export function createPost (props) {
  const request = axios.post(`${ROOT_URL}/posts${API_KEY}`, props);

  return dispatch => {
    return request.then(posts=>{
      dispatch({
        type: CREATE_POST,
        payload: posts
      })
    })
  };
}

export function fetchPost (id) {
  const request = axios.get(`${ROOT_URL}/posts/${id}${API_KEY}`);

  return dispatch => {
    request.then(posts=>{
      dispatch({
        type: FETCH_POST,
        payload: posts
      })
    })
  };
}

export function deletePost (id) {
  const request = axios.delete(`${ROOT_URL}/posts/${id}${API_KEY}`);

  return dispatch => {
    return request.then(posts=>{
      dispatch({
        type: DELETE_POST,
        payload: posts
      })
    })
  };
}